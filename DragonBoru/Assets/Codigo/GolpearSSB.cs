﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GolpearSSB : MonoBehaviour
{
    int kombo;
    int komboP;
    int kiAtack;
    public GameObject kame;
    public Animator animator;
    public Transform zonaAtaque;
    public float rangoAtaque = 0.5f;
    public LayerMask enemigo;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.J))
        {
            StartCoroutine(combo1("j"));
        }
        if (Input.GetKeyDown(KeyCode.L))
        {
            StartCoroutine(combo1("l"));
        }

        if (Input.GetKeyDown(KeyCode.K))
        {
            StartCoroutine(combo1("k"));
        }

        if (Input.GetKey(KeyCode.H))
        {
            animator.SetBool("bloqueo", true);
        }
        else if(!Input.GetKey(KeyCode.H))
        {
            animator.SetBool("bloqueo", false);  
        }
    }

    IEnumerator combo1(string tecla)
    {
        //Kombo de piernas primero
        if (tecla == "k" && komboP == 0 && kombo == 0)
        {
            //Primera patada para que no se buge
            animator.SetTrigger("Patada1");
            komboP = 1;
            yield return new WaitForSeconds(0.6f);
            if (komboP == 1)

            {
                komboP = 0;
            }
        }
        else if(tecla == "k" && komboP == 1 && kombo == 0)
        {
            animator.SetTrigger("Patada2");
            komboP = 2;
            yield return new WaitForSeconds(0.6f);
            if (komboP == 2)
            {
                komboP = 0;
            }
        }
        else if (tecla == "k" && komboP == 2 && kombo == 0)
        {
            animator.SetTrigger("Patada3");
            komboP = 3;
            yield return new WaitForSeconds(0.6f);
            if (komboP == 3)
            {
                komboP = 0;
            }
        }
        else if(tecla == "j" && komboP == 3)
        {
            animator.SetTrigger("Puño1");
            komboP = 4;
            yield return new WaitForSeconds(0.6f);
            if (komboP == 4)
            {
                komboP = 0;
            }
        }
        else if(tecla == "j" && komboP == 4)
        {
            animator.SetTrigger("Puño2");
            komboP = 5;
            yield return new WaitForSeconds(0.6f);
            if (komboP == 5)
            {
                komboP = 0;
            }
        }
        else
        {
            komboP = 0;
        }
        //Kombo de puños primero
        if (tecla == "j" && kombo == 0)
        {
            animator.SetTrigger("Puño1");
            kombo = 1;
            yield return new WaitForSeconds(0.6f);
            if (kombo == 1)

            {
                kombo = 0;
            }
        }
        else if (tecla == "j" && kombo == 1)
        {
            animator.SetTrigger("Puño2");
            kombo = 2;
            yield return new WaitForSeconds(0.6f);
            if (kombo == 2)
            {
                kombo = 0;
            }
        }
        else if (tecla == "k" && kombo == 2)
        {
            animator.SetTrigger("Patada1");
            kombo = 3;
            yield return new WaitForSeconds(0.6f);
            if (kombo == 3)
            {
                kombo = 0;
            }
        }
        else if (tecla == "k" && kombo == 3)
        {
            animator.SetTrigger("Patada2");
            kombo = 4;
            yield return new WaitForSeconds(0.6f);
            if (kombo == 4)
            {
                kombo = 0;
            }
        }
        else if (tecla == "k" && kombo == 4)
        {
            animator.SetTrigger("Patada3");
            kombo = 5;
            yield return new WaitForSeconds(0.8f);
            if (kombo == 5)
            {
                kombo = 0;
            }
        }
        else if (tecla == "l" && kombo == 5)
        {
            animator.SetBool("CargaKi", true);
            kiAtack = 6;
            Instantiate(kame, transform.position, Quaternion.identity);
            yield return new WaitForSeconds(0.8f);
            if (kiAtack == 6)
            {
                animator.SetBool("CargaKi", false);
                kiAtack = 0;
            }
        }
        else 
        {
            kombo = 0;
        }
    }
}
