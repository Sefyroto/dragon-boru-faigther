﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movimiento : MonoBehaviour
{
    public float velocidad;
    public float salto;
    bool saltar = true;
    Rigidbody2D rb;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        float horizontal = Input.GetAxis("Horizontal1"); 

        rb.velocity = new Vector2(horizontal * velocidad, rb.velocity.y); 

        if (Input.GetKeyDown("space") && saltar) 
        {
            rb.AddForce(new Vector2(0, salto * this.GetComponent<Rigidbody2D>().gravityScale));
            saltar = false;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "suelo") //si toca suelo cambia a true para poder saltar
        {
            saltar = true;
        }
    }

}
