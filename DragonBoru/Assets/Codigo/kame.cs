﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class kame : MonoBehaviour
{
    private Transform objetivo;
    public float velocidad = 1f;
    public float velocidadRotacion = 200f;
    private Rigidbody2D rb;

    void Awake()
    {
        if (this.gameObject.tag == "Goku") {
            objetivo = GameObject.Find("GokuBlack").gameObject.transform;
        }
        else if (this.gameObject.tag == "GokuBlack")
        {
            objetivo = GameObject.Find("GokuSSB").gameObject.transform;
        }

        rb = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        float step = velocidad * Time.deltaTime;
        transform.position = Vector3.MoveTowards(transform.position, objetivo.position, step);
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
       if (collision.gameObject.name.Equals("GokuBlack") && this.gameObject.tag == "Goku") 
       {
            collision.gameObject.GetComponent<vida>().Damages(10);
            Destroy(gameObject);
       }
        if (collision.gameObject.name.Equals("GokuSSB") && this.gameObject.tag == "GokuBlack")
        {
            collision.gameObject.GetComponent<vida>().Damages(10);
            Destroy(gameObject);
        }
    }
}
