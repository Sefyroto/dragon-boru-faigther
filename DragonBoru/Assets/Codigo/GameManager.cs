﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public GameObject gokuSSB;
    public Text gokuSSBVida;
    public GameObject gokuB;
    public Text gokuBVida;

    void Start()
    {
        if(SceneManager.GetActiveScene().name == "torneo")
        {
            gokuSSB.GetComponent<vida>().eventoVida += cambiarVida;
            gokuB.GetComponent<vida>().eventoVida += cambiarVida;
            cambiarVida();
        } 
    }

    void cambiarVida()
    {
        gokuSSBVida.text = "Vida " + gokuSSB.GetComponent<vida>().vidaActual;
        gokuBVida.text = "Vida " + gokuB.GetComponent<vida>().vidaActual;

        if (gokuB.GetComponent<vida>().vidaActual < 0)
        {
            SceneManager.LoadScene("MenuPrincipal");
        }
        else if (gokuSSB.GetComponent<vida>().vidaActual < 0)
        {
            SceneManager.LoadScene("MenuPrincipal");
        }
    }
}
