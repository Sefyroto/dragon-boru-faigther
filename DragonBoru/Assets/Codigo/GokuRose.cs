﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class GokuRose : MonoBehaviour
{
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.name.Equals("GokuX"))
        {
            SceneManager.LoadScene("MenuPrincipal");
        }
        if (collision.gameObject.name.Equals("Goku1"))
        {
            this.gameObject.GetComponent<Rigidbody2D>().gravityScale = -1;
        }
        if (collision.gameObject.name.Equals("Goku2"))
        {
            this.gameObject.GetComponent<Rigidbody2D>().mass = 1;
        }
        if (collision.gameObject.name.Equals("Goku3"))
        {
            this.gameObject.GetComponent<movimiento>().velocidad = 10;
            Time.timeScale = 0.5f;
        }
        if (collision.gameObject.name.Equals("Goku4"))
        {
            this.gameObject.GetComponent<SpriteRenderer>().flipY = true;
        }
        if (collision.gameObject.name.Equals("Goku5"))
        {
            this.gameObject.GetComponent<Rigidbody2D>().gravityScale = 2;
            Time.timeScale = 1f;
        }
        if (collision.gameObject.name.Equals("Goku6"))
        {
            this.gameObject.GetComponent<SpriteRenderer>().flipX = true;
            
        }
    }
}
