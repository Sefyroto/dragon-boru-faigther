﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class vida : MonoBehaviour
{
    public Animator animator;
    int vidaMax;
    public int vidaActual;

    public delegate void delegadoVida();
    public event delegadoVida eventoVida;

    void Update()
    {
        if (Input.GetKeyDown("p"))
        {
            Damages(4);
        }
    }

    public void Damages(int damage) 
    {
        vidaActual -= damage;
        if (eventoVida != null)
        {
            eventoVida();
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.name == "Golpe")
        {
            if (animator.GetBool("bloqueo"))
            {
                Damages(1);
            }
            else
            {
                Damages(5);
            }
            print("golpeado");
        }
        if (collision.name == "Patada")
        {
            if (animator.GetBool("bloqueo"))
            {
                Damages(0);
            }
            else
            {
                Damages(4);
            }
            print("golpeado");
        }
    }
}
