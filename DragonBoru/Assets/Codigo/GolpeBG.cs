﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GolpeBG : MonoBehaviour
{
    int kombo;
    int komboS;
    int kiAtack;
    public GameObject MicroKamehameha;
    public Animator animator;
    public Transform zonaAtaque2;
    public float rangoAtaque = 0.5f;
    public LayerMask aliado;

    // Update is called once per frame
    void Update()
    {
        {
            if (Input.GetKeyDown(KeyCode.Keypad1))
            {
                StartCoroutine(combo1("1"));
            }
            if (Input.GetKeyDown(KeyCode.Keypad2))
            {
                StartCoroutine(combo1("2"));
            }

            if (Input.GetKeyDown(KeyCode.Keypad3))
            {
                StartCoroutine(combo1("3"));
            }
            
            if (Input.GetKey(KeyCode.Keypad0))
            {
                animator.SetBool("bloqueo", true);
            }
            else if (!Input.GetKey(KeyCode.Keypad0))
            {
                animator.SetBool("bloqueo", false);
            }
        }

        IEnumerator combo1(string tecla)
        {
            //Combo puñetazos
            if (tecla == "1" && kombo == 0)
            {
                animator.SetTrigger("Combo1");
                kombo = 1;
                yield return new WaitForSeconds(0.6f);
                if (kombo == 1)

                {
                    kombo = 0;
                }
            }
            else if (tecla == "1" && kombo == 1)
            {
                animator.SetTrigger("Combo2");
                kombo = 2;
                yield return new WaitForSeconds(0.6f);
                if (kombo == 2)
                {
                    kombo = 0;
                }
            }
            else if (tecla == "1" && kombo == 2)
            {
                animator.SetTrigger("Combo3");
                kombo = 3;
                yield return new WaitForSeconds(0.6f);
                if (kombo == 3)
                {
                    kombo = 0;
                }
            }
            else if (tecla == "1" && kombo == 3)
            {
                animator.SetTrigger("Combo4");
                kombo = 4;
                yield return new WaitForSeconds(0.6f);
                if (kombo == 4)
                {
                    kombo = 0;
                }
            }
            else if (tecla == "1" && kombo == 4)
            {
                animator.SetTrigger("Combo5");
                kombo = 5;
                yield return new WaitForSeconds(0.6f);
                if (kombo == 5)
                {
                    kombo = 0;
                }
            }
            else if (tecla == "1" && kombo == 5)
            {
                animator.SetTrigger("Combo6");
                kombo = 6;
                yield return new WaitForSeconds(0.6f);
                if (kombo == 6)
                {
                    kombo = 0;
                }
            }
            else
            {
                kombo = 0;
            }

            //Combo Sable
            if (tecla == "2" && komboS == 0)
            {
                animator.SetTrigger("Sable1");
                komboS = 1;
                yield return new WaitForSeconds(0.6f);
                if (komboS == 1)
                {
                    komboS = 0;
                }
            }
            else if(tecla == "2" && komboS == 1)
            {
                animator.SetTrigger("Sable2");
                komboS = 2;
                yield return new WaitForSeconds(0.6f);
                if (komboS == 2)

                {
                    komboS = 0;
                }
            }
            else if(tecla == "2" && komboS == 2)
            {
                animator.SetTrigger("Sable3");
                komboS = 3;
                yield return new WaitForSeconds(0.6f);
                if (komboS == 3)
                {
                    komboS = 0;
                }
            }
            else if(tecla == "2" && komboS == 3)
            {
                animator.SetTrigger("Sable4");
                komboS = 4;
                yield return new WaitForSeconds(0.8f);
                if (komboS == 4)
                {
                    komboS = 0;
                }
            }
            else if (tecla == "3" && komboS == 4)
            {
                animator.SetBool("CargaKi", true);
                kiAtack = 5;
                yield return new WaitForSeconds(0.8f);
                Instantiate(MicroKamehameha, transform.position, Quaternion.identity);
                if (kiAtack == 5)
                {
                    animator.SetBool("CargaKi", false);
                    kiAtack = 0;
                }
            }
            else
            {
                komboS = 0;
            }
        }
    }
}